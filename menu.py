import pygame
from pygame.locals import *

number_buttons = {  K_0:0,       
					K_1:1,
					K_2:2,
					K_3:3,
					K_4:4,
					K_5:5,
					K_6:6,
					K_7:7,
					K_8:8,
					K_9:9,
					K_KP0:0,
					K_KP1:1,
					K_KP2:2,
					K_KP3:3,
					K_KP4:4,
					K_KP5:5,
					K_KP6:6,
					K_KP7:7,
					K_KP8:8,
					K_KP9:9}	
			
class button (object):	#Menu Button
	def __init__(self, rect, text, funk, colour = [200,20,25] ):
		self.rect = rect
		self.s_rect = rect.inflate(2,2)
		text = text
		font_size = int(self.rect.width/len(text))+5
		self.font = pygame.font.SysFont("Calibri", font_size)
		self.label = self.font.render(text, 1, (0,0,0))
		self.funk = funk
		self.colour = colour
		self.s_colour = [colour[1]/2,colour[2]/2,colour[2]/2] 
		
	def draw(self, update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.surf_GUI, self.colour,self.rect,0)
		pygame.draw.rect(PROGRAM.surf_GUI, self.s_colour,self.s_rect,1)
		
		PROGRAM.surf_GUI.blit(self.label, ((self.rect.left+self.rect.width/2) - self.label.get_width()/2, (self.rect.top+self.rect.height/2) - self.label.get_height()/2))
		if update:
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.updates.append(self.s_rect)
		
	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		f = self.funk[:]
		if "ONETIME" in f:
			f.remove("ONETIME")
			one_time_funk = f[0]
			f.pop(0)
			one_time_funk(*f)
			f = []
		else:
			PROGRAM.funktion = f[0]

	
class menu_box (object):
	def __init__(self,rect,content,colour):
		self.rect = rect
		self.content = content
		self.colour = colour
		
	def draw(self, update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.surf_GUI, self.colour ,self.rect,0)
		for x in self.content:
			x.draw(update = False)
		if update:
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.updates.append(self.rect)
	
	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		for x in self.content:
			if x.pressed(PROGRAM.mouse[0]):
				x.act()
				break
				
class input_box(object):
	def __init__(self,rect,explanation,string):
		self.string = str(string)
		self.contex = explanation
		self.rect = rect
		self.s_rect = self.rect.inflate(2,2) #surrounding rect
		self.active = False
		
		self.font_size = 18
		self.font = pygame.font.SysFont("Calibri", self.font_size)		
		self.label = self.font.render(self.string, 1, (255,255,255))
		self.explanation = self.font.render(self.contex, 1, (244,0,0))
		
		
	def pressed(self,mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		PROGRAM.active_text_field = self
		
	def update(self):
		
		from game import PROGRAM
		for key in PROGRAM.keys:
			if key == 47 or key == K_KP_MINUS: 
				if self.string == "":
					self.string = "-"
			elif key == K_BACKSPACE:
				self.string = self.string[0:-1]
			else:
				try:
					self.string += str(number_buttons[key])
				except KeyError:
					pass
		self.label = self.font.render(self.string, 1, (255,255,255))
		self.draw()
	def draw(self, update = True):
		from game import PROGRAM
		#clean
		pygame.draw.rect(PROGRAM.surf_GUI, (255,255,255),self.s_rect, 1)		
		
		pygame.draw.rect(PROGRAM.surf_GUI, (0,0,0),self.rect, 0)
		
		e_pos = map(sum, zip(self.rect.topleft, (0,-4-self.font_size))) 
		ex_rect = PROGRAM.surf_GUI.blit(self.explanation,e_pos)
		#print text
		if len(self.string) != 0:
			pos = map(sum, zip(self.rect.topleft, (2,2))) 
			PROGRAM.surf_GUI.blit(self.label,pos)
		#draw writing surface
			
		PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.s_rect, self.s_rect)
		PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, ex_rect, ex_rect)
		PROGRAM.updates.append(self.s_rect)

			
			
			
			
			
					