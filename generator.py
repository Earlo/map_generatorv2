import random
import math
import pygame

import perlin
import vector
#debug
import image

import grid_tile
import world


#generatio constants
max_continents = 16
continent_growth_chance_equalizer = 2
def rotate(l,n):#list, number of spaces
    return l[n:] + l[:n]

class generator(object):
	def __init__(self,seed,num_continents,size):
		print "Starting Generation!"
		self.seed = seed
		random.seed(seed)
		if num_continents > max_continents:
			print "please don't try to generate more than",max_continents,"continents"
			num_continents = max_continents
		self.num_continents = num_continents
			
		self.world = world.world(size)
		self.continents = []
		while len(self.continents) < self.num_continents:
			r1 = random.randint(0,self.world.size[0]-1)
			r2 = random.randint(0,self.world.size[1]-1)
			t_tile = self.world.tiles[r1][r2]
			condition = True
			for check in self.continents:
				if t_tile in check.tiles:
					condition = False
			if condition:
				self.continents.append(continent(t_tile,self.world))
		
		self.open_continents = self.continents #this is needed for continent defining
		self.current = self.define_continents
		self.cont_sample_len = int(math.sqrt(len(self.open_continents)))
		#if self.cont_sample_len == 0:
		#	self.cont_sample_len = 1
	
	def define_continents(self):
		start_t = pygame.time.get_ticks()
		
		while (pygame.time.get_ticks() - start_t < 34 and len(self.open_continents) > 0 ):
			c_candidates = random.sample(self.open_continents,self.cont_sample_len)#current continent cc
			cc = max (c_candidates, key=lambda x: len(x.openlist)/len(x.tiles) )
			#cc = max (self.open_continents, key=lambda x: len(x.openlist)/len(x.tiles) )
			#cc = self.open_continents[0]
			try:
				#t = cc.openlist[0]		
				samp = random.sample(cc.openlist,int(math.sqrt(len(cc.openlist))))
				t = min(samp, key=lambda x: vector.vlen(cc.rel_pos[x])) #oo slow
			#except IndexError:
			except ValueError:
				self.open_continents.remove(cc)
				self.cont_sample_len = int(math.sqrt(len(self.open_continents)))
				break
				
			for key,ct in t.next.iteritems():
				free = True
				#for continent in self.continents:
				#	try: 						#ugly and hard to read fast way to check
				#		continent.rel_pos[ct]
				#		free = False
				#		break
				#	except KeyError:
				#		pass
				#	if ct in continent.tiles: #easy to understand and pretty slow way to check
				#		free = False
				#		break
				#if free:
				if ct.continent == None:
					cc.openlist.append(ct)
					cc.tiles.append(ct)
					ct.continent = cc
					cc.rel_pos[ct] = [cc.rel_pos[t][0] + key[0], cc.rel_pos[t][1] + key[1]]
				elif not ct.continent == cc:
					try:
						cc.border_tiles[ct.continent].append(t)
					except KeyError:
						cc.border_tiles[ct.continent] = [t]
			cc.openlist.remove(t)
			#old = self.continents
			self.open_continents = rotate(self.open_continents,1)
			#print old == self.continents
			
		if len(self.open_continents) == 0:
			self.current = self.base_noise

			print "defined continents, starting generation heigthmaps"
			
			self.open_continents = self.continents[:] #reset the iterable continent list
			for c in self.continents:
				c.openlist = c.tiles[:] #reset list of unedited tiles
				#c.min_x c.min_y
				c.calculate_dimensions()

			self.uniform_noise = perlin.SimplexNoise(self.seed)
			self.scale = 1.25
			self.amplitude = 120 
			
	def base_noise(self):
		start_t = pygame.time.get_ticks()
		while (pygame.time.get_ticks() - start_t < 34 and len(self.open_continents) > 0 ):
			cc = self.open_continents[0]#current continent
			try:
				t = cc.openlist[0]				
			except IndexError:
				self.open_continents.remove(cc)
				break
			base_e = cc.base_elevation(t)
			perl_e = 0
			for amp_divider,sca_divider in [[1,120],[8,40],[24,12],[72,4]]:
				sca = self.scale/sca_divider
				amp = self.amplitude/amp_divider
				perl_e += self.uniform_noise.noise2(t.pos[0]*sca,t.pos[1]*sca) * amp
			h = base_e + perl_e
			if h > 255:
				h = 255
			elif h < -255:
				h = -255
			t.ele = h
			#print h
			cc.openlist.remove(t)
		if len(self.open_continents) == 0:	
			del self.open_continents # = self.continents[:]

			#debug
			output_image(self.continents,self.seed,self.world)

			self.current = self.erosion
			self.c_weld = 5 #iterations of continental_weld to come
			self.iterations = 10 #current, to come, max
			self.x_i = 0 #x index
			self.y_i = 0 #y index
			self.slope_limit = 10
			self.border_tiles = []
		#	for continent in self.continents:
		#		self.border_tiles.extend(continent.border_tiles.values()[0])
		#		self.border_tiles.extend(continent.border_tiles.values()[0])
			print "done with heigth mapping"	
							
	def continental_collision(self):
		pass
		
	def continental_weld(self):
		start_t = pygame.time.get_ticks()
		while (pygame.time.get_ticks() - start_t < 34 and len(self.border_tiles) > 0 ):		
			t = self.border_tiles[0] #tile in question
			for nt in t.next.values():
				de = nt.ele - t.ele #difference in elevation relative to center tile
				change = de/2
				t.ele += change
				nt.ele -= change
			self.border_tiles.remove(t)	
		if len(self.border_tiles) == 0:	
			#debug
			#output_image(self.continents,self.seed,self.world,extra_str ="welt")
			self.current = self.erosion
			if self.c_weld == 0:
				del self.border_tiles
				print "done with continent welding"	
			
	def erosion(self):
		start_t = pygame.time.get_ticks()
		while (pygame.time.get_ticks() - start_t < 34 and self.iterations > 0 ):
			t = self.world.tiles[self.x_i][self.y_i] #tile in question
			for nt in t.next.values():
				de = nt.ele - t.ele #difference in elevation relative to center tile
				if math.fabs(de) < self.slope_limit:
					change = de/2
					t.ele += change
					nt.ele -= change


			self.y_i += 1
			if self.y_i + 1 >= len( self.world.tiles[self.x_i] ):
				self.y_i = 0
				self.x_i += 1
				if self.x_i + 1 >= len( self.world.tiles ):		
					self.x_i = 0
					self.iterations -= 1
					if self.iterations == 0:
						#debug
						output_image(self.continents,self.seed,self.world,extra_str ="eroded")
						output_image(self.continents,self.seed,self.world,extra_str ="continents",continents = True)

						self.current = self.biomes
						del self.iterations #current, to come, max
						del self.x_i #x index
						del self.y_i #y index
						del self.slope_limit 
						print "done with erosion mapping"	
					elif self.c_weld > 0:
						for continent in self.continents:
							self.border_tiles.extend(continent.border_tiles.values()[0])
						self.current = self.continental_weld
						self.c_weld -= 1
						
	def biomes(self):
		pass
		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~end of generator
class continent():
	def __init__(self,tile,parent):
		self.parent = parent
		self.tiles = []
		self.openlist = []
		self.border_tiles = {} #continent : [tiles]
		self.openlist.append(tile)
		self.tiles.append(tile)
		self.rel_pos = {tile: [0,0]}
		tile.continent = self
		self.amplitude = 125
		
		#debug
		#self.r = random.randint(0,255)
		self.p = 1.2
		#self.hue = [random.randint(0,250),random.randint(0,250),random.randint(0,250)]
		#self.origin = tile
	def calculate_dimensions(self):
		cen_x = sum( [t[0] for t in self.rel_pos.values()] ) / len(self.tiles)
		cen_y = sum( [t[1] for t in self.rel_pos.values()] ) / len(self.tiles)
		for key in self.rel_pos.keys():
			self.rel_pos[key] = [self.rel_pos[key][0]-cen_x,self.rel_pos[key][1]-cen_y]
		max_x = max (self.rel_pos.iteritems(), key=lambda item: item[1][0])[1][0]
		max_y = max (self.rel_pos.iteritems(), key=lambda item: item[1][1])[1][1]
		self.max_diff = math.sqrt(float(max_x**self.p + max_y**self.p))
	def base_elevation(self,tile): #To be tweaked with
		x_os,y_os = self.rel_pos[tile]
		#dis = math.sqrt(y_os**2 + x_os**2)
		#debug
		try:
			diff = 2*(math.sqrt(math.fabs(y_os)**self.p + math.fabs(x_os)**self.p)/self.max_diff) 
		except ZeroDivisionError:
			print len(self.tiles),"tiles"
			return None
		value = 1.0 - diff

		return value*self.amplitude
			
			
def output_image(con,seed,world,extra_str = "",continents = False):		
	#debug
	surf = pygame.Surface(world.size)
	for c in con:
		for tile in c.tiles:
			if tile.ele > 0:
				g = 255-int(tile.ele)
				if g < 50:
					g = 50
				col = [0,g,0]
			else:
				b = 255-math.fabs(int(tile.ele))				
				if b < 50:
					b = 50
				col = [0,0,b]
			surf.set_at(tile.pos, col)
	if continents:
		for c in con:
			for b in c.border_tiles.values():
				for tile in b:
					surf.set_at(tile.pos, (255,0,0))
		
	pygame.image.save(surf, "world"+str(seed)+extra_str+".png")
		
			
			
			
			