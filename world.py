import math

import grid_tile




class world(object):
	def __init__(self,size):
		
		self.size = size
		self.longest_dist = math.sqrt(size[0]**2 + size[1]**2)
		self.tiles = []		
		for x in range(0,size[0]):
			self.tiles.append([])
			for y in range(0,size[1]):
				self.tiles[x].append(grid_tile.tile([x,y], self))
		t_os = size[0]/2
		for line in self.tiles:
			for tile in line:
				tile.connections(t_os)
				
				
				