
import pygame
import random

class tile(object):
	def __init__(self, pos, world, FLAGS = [] , colour = (0,0,0) ):
		
		self.content = []
		#self.node = None #these are used to generalize pathfinding to make it faster
		self.FLAGS = FLAGS
		#self.colour = (0,0,0)
		#shade = random.randint(0,30)
		#self.colour = (shade,shade,shade)
		self.colour = colour
		self.width = 0
		#self.rect = pygame.Rect(pos[0]*size,pos[1]*size,size,size)
		#self.size = size
		self.pos = pos
		self.world = world
		self.continent = None
		self.ele = 0
				
	def blit(self):
		from game import PROGRAM
		if PROGRAM.game_camera.contains(self.rect):
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			PROGRAM.MainWindow.blit(self.world.surf_bgr, srect.topleft, area = self.rect)
			PROGRAM.updates.append(srect)	
	
	def raw_blit(self):
		from game import PROGRAM
		if PROGRAM.game_camera.contains(self.rect):
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			PROGRAM.MainWindow.blit(self.world.surf_bgr, srect.topleft, area = self.rect)


		
	#def draw(self, update = True):
	def draw(self,update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.world.surf_obj,(255,255,255),self.rect,0)#remove anything from obj layer on the tile		
		if not self.occupier == None:
			self.draw_content(self.occupier)
		elif not self.content == []:
			self.draw_content(self.content[-1])
		else:
			pygame.draw.rect(PROGRAM.world.surf_bgr,self.colour,self.rect,self.width)		
			self.blit()	
																											# 4 1 5																									
																											# 0 x 2
																											# 7 3 6	
	def connections(self,t_os): #find adjatant tiles, Ugly and somewhat pointless, but, will do. #spherical!!!
		#print self.pos
		world = self.world
		self.next = {}
		#if self.pos[0] < world.size[0]-1 and self.pos[1] > 0 and self.pos[1] < world.size[1]-1:
		sl1,sl2 = self.pos[0]-1,self.pos[1]
		su1,su2 = self.pos[0],self.pos[1]-1
		sr1,sr2 = self.pos[0]+1,self.pos[1]
		sd1,sd2 = self.pos[0],self.pos[1]+1
		
		if self.pos[0] == world.size[0]-1: # right side
			sr1 = 0				
		if self.pos[1] == 0: # up side
			su1,su2 = self.pos[0]-t_os,self.pos[1]
		elif self.pos[1] == world.size[1]-1: # down side
			sd1,sd2 = self.pos[0]-t_os,self.pos[1]
			
		self.next[(-1, 0)] = world.tiles[sl1][sl2] #left
		self.next[( 1, 0)] = world.tiles[sr1][sr2] #right 			
		self.next[( 0,-1)] = world.tiles[su1][su2] #up 
		self.next[( 0, 1)] = world.tiles[sd1][sd2] #down
		
		#temporary retard solution
		if self.pos[1] == 0: # up side
			del self.next[( 0,-1)] 
		elif self.pos[1] == world.size[1]-1: # down side
			del self.next[( 0, 1)]


	