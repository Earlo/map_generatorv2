import pygame
import os
from pygame.locals import *
import random


import screen,menu,generator




class game():
	
	def __init__(self):
		
		self.FPS = 30 # 60 frames per second
		
		#self.FPS = 6 # 60 frames per second
		
		self.clock = pygame.time.Clock()
		SWIDTH =  1000
		SHEIGTH = 600	
		self.MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
		pygame.display.set_caption("Map generator interface")

		self.mouse = [pygame.mouse.get_pos() , False ,  [0,0] , None ]
		self.generations_parameters = { "Seed": random.randint(10000,99999),
										"Num_Continents": 5,
										"Dimensions[X,Y]": [460,320] #ratio around sqrt(2) for the most pleasing results
																			}
		self.game_keymap = {}
		
		self.active_text_field = None
		self.surf_GUI = pygame.Surface((SWIDTH, SHEIGTH))
		self.updates = []
		self.erase = []
		self.draw = []
		
		#path = os.path.join("recourses","cursor.bmp")
		
		self.funktion = None
		self.GUI = []
		self.done = False
		self.debug_message = ""
	
	def m_loop(self):
		while not self.done:
			
			#self.keys = [self.keys,pygame.key.get_pressed()] , key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
			self.keys = []
			self.mouse[0] = pygame.mouse.get_pos()
			self.mouse[1] = False
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					self.done = True # Stop the Loop
				if event.type == pygame.MOUSEBUTTONUP:
					self.mouse[1] = [True]
					for object in self.GUI:#check if any in game buttons are pressed
						if object.pressed(self.mouse[0]):
							object.act()
				if event.type == pygame.KEYDOWN:
					self.keys.append(event.key)
					try:
						self.run_command(self.game_keymap[event.key])
					except KeyError:
						pass
					
			#print len(self.keys), self.active_text_field
			if len(self.keys) > 0 and not self.active_text_field == None:
				self.active_text_field.update()
						#key pressed was mapped for nothing
			self.funktion()
			pygame.display.update(self.updates)			
			self.updates = []
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Start menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def sstart_menu(self):
		self.GUI = []
		buttons = []
		print "map with following parameters will be created"
		print self.generations_parameters
		buttons.append(menu.button(pygame.Rect(400,100,200,100),"Run Generator",[self.sgeneration]))
		buttons.append(menu.button(pygame.Rect(400,300,200,100),"Edit Parameters",[self.spara_editor]))
		menu_bar = menu.menu_box(pygame.Rect(300,50,400,400),buttons,(25,50,100))
		self.GUI.append(menu_bar)
		
		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.start_menu
		
	def start_menu(self):
		pass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Stage select menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~			
	def spara_editor(self):
		self.GUI = []
		self.para_holders = []
		self.GUI.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100,160,80),"Main menu",[self.sstart_menu]))
		self.GUI.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-200,160,80),"Confirm changes",["ONETIME",self.edit_para]))
		
		x = 0
		y = 0
		for key in self.generations_parameters.keys():
			if type (self.generations_parameters[key]) == int:
				name = key
				obj = menu.input_box(pygame.Rect(10+100*x,20+80*y,80,20),name,self.generations_parameters[key])
				obj.parent = key
				self.GUI.append(obj)
				self.para_holders.append(obj)
			elif type (self.generations_parameters[key]) == list:
				bname,indexes = key.split('[')
				indexes = indexes[0:-1].split(",")  
				i = 0
				for val in self.generations_parameters[key]:
					index = indexes[i]
					if type (val) == int:
						name = bname+index
						obj = menu.input_box(pygame.Rect(10+100*x+i,20+80*y,80,20),name,self.generations_parameters[key][i])
						obj.parent = [key,index]
						self.GUI.append(obj)
						self.para_holders.append(obj)
					i+=1
					x += 1
					if x*100 > pygame.display.Info().current_w:
						x = 0
						y+= 1
						
			x += 1
			if x*100 > pygame.display.Info().current_w:
				x = 0
				y+= 1

		self.MainWindow.fill((100,100,100))
		
		self.refresh_GUI()
		
		pygame.display.flip()
		self.funktion = self.para_editor		
		
	def para_editor(self):
		pass
		
	def edit_para(self):
		for key,value in self.generations_parameters.items():
			if type (value) == int:
				for input in self.para_holders:
					if input.parent == key:
						self.generations_parameters[key] = int(input.string)
						break
			elif type (value) == list:
				indexes = key.split('[')[-1]
				indexes = indexes[0:-1].split(",")  
				for input in self.para_holders:
					if input.parent[0] == key:
						i = 0
						for index in indexes:
							if index == input.parent[1]:
								value[i] = int(input.string)
							i += 1
				self.generations_parameters[key] = value
				
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~generation setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def sgeneration(self):
		self.generator = generator.generator(*self.generations_parameters.values())
		self.funktion = self.generation		
	def generation(self):
		self.generator.current()#calls the current generation function in progress
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Pausemenu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def spmenu (self):
		self.game_keymap = {}
		self.game_keymap[K_ESCAPE] = [[self.rgame]]

		self.GUI = []
		
		buttons = []
		#buttons.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100,160,80),"Main menu",[self.sstart_menu]))
		buttons.append(menu.button(pygame.Rect(400,100,200,80),"Main menu",[self.sstart_menu]))
		
		buttons.append(menu.button(pygame.Rect(678, 52, 20,20),"X",[self.rgame]))

		pause_menu = menu.menu_box(pygame.Rect(300,50,400,400),buttons,(25,50,100))		
		
		self.GUI.append(pause_menu)
		
		self.refresh_GUI()
		self.funktion = self.pmenu
		
	def pmenu (self):
		pygame.display.update(self.updates)
		

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Game setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def sgame(self):
		#setting up important variables
		self.GUI = []
		self.draw =  []
		self.units = []
		self.game_keymap = {}
		self.turncount = 0

		#loading stage and setting it up
		self.stage = room.level(self.selected_stage)
		self.stage.connections()
		self.stage.create_rooms()
		self.game_camera = screen.Camera(self.stage.LevelRect, self.MainWindow.get_size()[0]-200, self.MainWindow.get_size()[1])
		
		#player
		self.PLAYER = None
		for row in self.stage.tiles:
			for tile in row:
				if not 'WALL' in tile.FLAGS and self.PLAYER == None:
					self.PLAYER =  unit.unit(tile, AI = False)
					break
		print "dodny"
		self.units.append(self.PLAYER)		
		
		#setting up GUI
		pause_button = menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180, 50, 160,50),"Pause Menu",[self.spmenu])
		
		sidebar_rect = pygame.Rect(self.MainWindow.get_size()[0]-200,0,200,self.MainWindow.get_size()[1])
		sidebar_buttons = []
		sidebar_buttons.append(self.PLAYER.inventory)
		sidebar_buttons.append(self.PLAYER.surrounding_loot)
		sidebar_buttons.append(pause_button)
		sidebar = menu.menu_box(sidebar_rect,sidebar_buttons,(50,50,50))
		PROGRAM.GUI.append(sidebar)
		
		#setting keys
		self.game_keymap[K_ESCAPE] = [[self.spmenu]]

		#setting up NPC's debug mostly
		for x in range(0,0):
			#debug
			enemy = unit.unit(self.stage.tiles[5+x*10][5+x*10])
			#enemy.behaviour = behaviour.pathfind_tester
			enemy.behaviour = [behaviour.wanderer]
			#enemy.target = self.PLAYER
			enemy.speed = 1
			enemy.colour  = (255,155,55)
			
			self.units.append(enemy)
			
		#enemy = unit.unit(self.stage.tiles[2][1])
		#enemy.behaviour = behaviour.pathfind_tester
		#enemy.behaviour = [behaviour.pathfind_tester]
		#enemy.behaviour = [behaviour.stalker]
		#enemy.memory[self.PLAYER] = char_memories.unit_info(self.PLAYER)
		#enemy.memory[self.PLAYER].tile = [self.PLAYER.tile,0]
		#enemy.target = enemy.memory[self.PLAYER]
		#enemy.speed = 2
		#enemy.colour  = (155,55,55)
		
		#self.units.append(enemy)	
		
		#setting up items in ground
	#	for x in range(0,50):
	#		i = items.item1(self.stage.tiles[20][20])
	#		self.stage.items.append(i)
	#	for x in range(0,50):
	#		i = items.item2(self.stage.tiles[21][20])
	#		self.stage.items.append(i)
		
		#drawing stuff
		self.stage.draw()
		self.refresh_GUI()
		for item in self.stage.items:
			item.setup()
				
		print "done setting up"
		self.funktion = self.game
		self.PLAYER.FOW()
		pygame.display.flip()			

	def rgame(self): #return to game
		self.game_keymap = {}
		self.GUI = []
		
		#setting up GUI
		#setting up GUI
		pause_button = menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180, 50, 160,50),"Pause Menu",[self.spmenu])
		
		sidebar_rect = pygame.Rect(self.MainWindow.get_size()[0]-200,0,200,self.MainWindow.get_size()[1])
		sidebar_buttons = []
		sidebar_buttons.append(self.PLAYER.inventory)
		sidebar_buttons.append(self.PLAYER.surrounding_loot)
		sidebar_buttons.append(pause_button)
		sidebar = menu.menu_box(sidebar_rect,sidebar_buttons,(50,50,50))
		PROGRAM.GUI.append(sidebar)
		
		
		#setting keys
		self.game_keymap[K_ESCAPE] = [[self.spmenu]]
		
		self.stage.blit_level(self.game_camera)
		pygame.display.flip()			
		self.funktion = self.game
		
	def game(self):	
		#player controls
		self.mouse[2] =  map(sum, zip( self.mouse[0] , self.game_camera.topleft ))
		if self.mouse[1]: 
			if self.game_camera.collidepoint(self.mouse[2]):#if in game screen
				t = self.mouse[2][0]/self.stage.tile_size , self.mouse[2][1]/self.stage.tile_size
				m_tile =  self.stage.tiles[t[0]][t[1]]
				if not "WALL" in m_tile.FLAGS and not "UNEXPLORED" in m_tile.FLAGS:
					if self.mouse[3] == None:
						self.PLAYER.target = self.stage.tiles[t[0]][t[1]]						
					else:
						if m_tile in self.PLAYER.tile.next.values() or m_tile == self.PLAYER.tile:
							inventory.transfer_item_to(m_tile , self.mouse[3])
							m_tile.draw()
							self.mouse[3] = None
							self.PLAYER.check_loot()
		x = 0
		max = len(self.units)
		start = pygame.time.get_ticks()
		current =  pygame.time.get_ticks() - start
		
		while (current < 16 and max > x):
	#	while (max > x):
	#	while (current < 16):
			#if max > x:
			turn_end = self.units[0].act()
			if turn_end:
				x += 1
				self.turncount += 1
				self.units.append(self.units[0])
				self.units.pop(0)
			current =  pygame.time.get_ticks() - start

		f = self.game_camera.update(self.PLAYER.tile.rect.topleft)
		for obj in self.draw:
			obj.draw()

		self.draw =  []

		if not f:
			pygame.display.update(self.updates)
		else:
			#print "kyyly"
			#for item in self.stage.items:
			#	item.draw()
			pygame.display.flip()	
		#pygame.display.flip()			
		self.updates = []
		self.clock.tick(self.FPS)
		#pygame.display.set_caption("FPS: %i" % self.clock.get_fps())
		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ General Game Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	def run_command(self, commands):
		for c in commands:
			command = c[:]
			if command[0] == "ONETIME":
				command.pop(0)
				f = command[0]
				command.pop(0)
				f(*command)
			else:
				self.funktion = command[0]
				
	def refresh_GUI(self):
		for object in self.GUI:
			object.draw()
			
	def add_to_(self,object,list):
		list.append(object)



def start():
	global PROGRAM
	PROGRAM = game()
	PROGRAM.funktion = PROGRAM.sstart_menu
	PROGRAM.m_loop()
	pygame.quit()		